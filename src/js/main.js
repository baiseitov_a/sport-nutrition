
$(document).ready(function(){

  $('.slider .owl-carousel').owlCarousel({
    loop:true,
    items: 1,
    nav:true,
    dots: true,
    navText:[]
	});

	$('.catalogs-block .owl-carousel').owlCarousel({
    loop:true,
    items: 5,
    nav:true,
    navText:[]
	});


   $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', 
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300
        }
    });

    $input = $('.price-quantity input');
    $total = $('.price-total b');
    $price = $('.price-money b');

   $input.change(function(){

    $piece = parseInt($price.text());    
    $value = parseInt($input.val());

    if(!isNaN($value)){
        $summ = $piece * $value;
        $total.text($summ + ' тг.');
    }
    else{
        $total.text('0 тг.');
    }        

   });

        

});

